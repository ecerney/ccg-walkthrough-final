//
//  Card.swift
//  RWCCG
//
//  Created by Brian Broom on 7/22/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

import Foundation
import SpriteKit

enum CardName: Int {
    case CreatureWolf = 0,
    CreatureBear,       // 1
    CreatureDragon,     // 2
    Energy,             // 3
    SpellDeathRay,      // 4
    SpellRabid,         // 5
    SpellSleep,         // 6
    SpellStoneskin      // 7
}

class Card : SKSpriteNode {
    
    let frontTexture :SKTexture
    let backTexture :SKTexture
    var largeTexture :SKTexture?
    let largeTextureFilename :String
    var faceUp = true
    var enlarged = false
    var savedPosition = CGPointZero
    
    init(cardNamed: CardName) {
        
        // initialize properties
        backTexture = SKTexture(imageNamed: "card_back.png")
        
        switch cardNamed {
        case .CreatureWolf:
            frontTexture = SKTexture(imageNamed: "card_creature_wolf.png")
            largeTextureFilename = "card_creature_wolf_large.png"
            
        case .CreatureBear:
            frontTexture = SKTexture(imageNamed: "card_creature_bear.png")
            largeTextureFilename = "Card_creature_bear_large.png"
            
        default:
            frontTexture = SKTexture(imageNamed: "card_back.png")
            largeTextureFilename = "card_back_large.png"
        }
        
        // call designated initializer on super
        super.init(texture: frontTexture, color: nil, size: frontTexture.size())
        
        
        // set properties defined in super
        userInteractionEnabled = true
    }
    
    //MARK: Touch Handlers
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        for touch in touches {
            if touch.tapCount > 1 {
                enlarge()
            }
            
            if enlarged { return }
            
            // note: removed references to touchedNode
            // 'self' in most cases is not required in Swift
            zPosition = 15
            let liftUp = SKAction.scaleTo(1.2, duration: 0.2)
            runAction(liftUp)
            
            let wiggleIn = SKAction.scaleXTo(1.0, duration: 0.2)
            let wiggleOut = SKAction.scaleXTo(1.2, duration: 0.2)
            let wiggle = SKAction.sequence([wiggleIn, wiggleOut])
            let wiggleRepeat = SKAction.repeatActionForever(wiggle)
            
            // again, since this is the touched sprite
            // run the action on self (implied)
            runAction(wiggleRepeat, withKey: "wiggle")
        }
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
        if enlarged { return }
        
        for touch in touches {
            let location = touch.locationInNode(scene)  // make sure this is scene, not self
            position = location
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        if enlarged { return }
        
        for touch in touches {
            zPosition = 0
            
            let dropDown = SKAction.scaleTo(1.0, duration: 0.2)
            runAction(dropDown)
            
            removeActionForKey("wiggle")
        }
    }
    
    //MARK: Card Actions
    func flip() {
        let firstHalfFlip = SKAction.scaleXTo(0.0, duration: 0.4)
        let secondHalfFlip = SKAction.scaleXTo(1.0, duration: 0.4)
        
        setScale(1.0)
        
        if faceUp {
            runAction(firstHalfFlip) {
                self.texture = self.backTexture
                if let damageLabel = self.childNodeWithName("damageLabel") {
                    damageLabel.hidden = true
                }
                self.faceUp = false
                self.runAction(secondHalfFlip)
            }
        } else {
            runAction(firstHalfFlip) {
                self.texture = self.frontTexture
                if let damageLabel = self.childNodeWithName("damageLabel") {
                    damageLabel.hidden = false
                }
                self.faceUp = true
                self.runAction(secondHalfFlip)
            }
        }
    }
    
    func enlarge() {
        if enlarged {
            let slide = SKAction.moveTo(savedPosition, duration:0.3)
            let scaleDown = SKAction.scaleTo(1.0, duration:0.3)
            runAction(SKAction.group([slide, scaleDown])) {
                self.enlarged = false
                self.zPosition = 0
            }
        } else {
            enlarged = true
            savedPosition = position
            
            if largeTexture {
                texture = largeTexture
            } else {
                largeTexture = SKTexture(imageNamed: largeTextureFilename)
                texture = largeTexture
            }
            
            zPosition = 20
            
            let newPosition = CGPointMake(CGRectGetMidX(parent.frame), CGRectGetMidY(parent.frame))
            removeAllActions()
            
            let slide = SKAction.moveTo(newPosition, duration:0.3)
            let scaleUp = SKAction.scaleTo(5.0, duration:0.3)
            runAction(SKAction.group([slide, scaleUp]))
        }
    }
}